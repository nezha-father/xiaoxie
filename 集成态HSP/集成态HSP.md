# 集成态HSP
HSP（Harmony Shared Package）是动态共享包，可以包含代码、C++库、资源和配置文件，通过HSP可以实现代码和资源的共享。
当前HSP分为两种：
- 应用内HSP:HSP不支持独立发布，而是跟随其宿主应用的APP包一起发布，与宿主应用同进程，具有相同的包名和生命周期。
- 集成态HSP:构建、发布过程中，不与特定的应用包名耦合；使用时，工具链支持自动将集成态HSP的包名替换成宿主应用包名。
 
应用内HSP与集成态HSP的区别：（如下表所示）
| 规格 | 应用内HSP | 集成态HSP |
|--------- | -------- | -------- |
| 支持在配置文件中声明UIAbility组件和ExtensionAbility组价 | 不支持 | 不支持 |
| 打包方式 | 随宿主应用进行打包 | 编译后可以跨应用进行打包 |
| 配置文件 | 默认方式 |  默认的模版需要进行 |


## 集成态HSP的使用场景
集团内部有多个应用，多个应用中都有一个相同的动态共享包。为了节约开发成本、实现代码和资源的共享，多个应用可以共享一个基建HSP(集成态HSP)。

## 集成态HSP的使用限制
- 集成态HSP只支持Stage模型。
- 集成态HSP需要在API12及以上版本使用，并且使用标准化的OHMUrl格式。

## 运行环境
本例基于以下开发环境：
- IDE:DevEco Studio 5.0.3.403
- SDK:API version 12

## 集成态HSP的开发
1. 创建一个新的工程（集成态HSP创建方）。
![创建工程1](./images/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%201.png)
![创建工程2](./images//%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202.png)
    新增配置，在工程级的目录下build-profile.json5。
    ![创建HSP1](./images//%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%203.png)
    ![创建HSP2](./images/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%204.png)
2. 新建HSP并进行配置，点击file->New->Moudle。
    新增配置，在HSP模块的目录下build-profile.json5。
    ![创建HSP3](./images//%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%205.png)
    ![创建HSP4](./images//%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%2020.png)
3. 获取集成态的编译产物.tgz。
![编译打包1](./images//%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE21.png)
![编译打包2](./images//%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%208.png)
    编译打包生成.tgz文件
    ![编译打包3](./images//%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%209.png)



